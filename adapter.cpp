#include <iostream>

// 変更できないクラス (Adaptee)
class DasaiClass
{
private:
  int atai;

public:
  DasaiClass(int value) : atai(value) {}

  int ataiWoKaesu() const
  {
    return this->atai;
  }
};

// このインターフェースに修正したい (Target)
class IketeruClass
{
public:
  virtual int getValue() const = 0;
};

// Adapterクラス
class IketeruClassAdapter : public IketeruClass, private DasaiClass
{
public:
  IketeruClassAdapter(int value) : DasaiClass(value) {}

  int getValue() const override
  {
    return this->ataiWoKaesu();
  }
};

int main()
{
  auto c = IketeruClassAdapter(7);
  std::cout << c.getValue() << std::endl;
}