CC := g++
CFLAGS := -std=c++17 -Wall
OUTDIR := build

.PHONY: all
all: abstract-factory adapter bridge decorator facade factory-method observer singleton strategy template-method class

abstract-factory: abstract-factory.cpp
	@if [ ! -d $(OUTDIR) ]; then mkdir $(OUTDIR); fi
	$(CC) -o $(OUTDIR)/abstract-factory $(CFLAGS) abstract-factory.cpp
adapter: adapter.cpp
	@if [ ! -d $(OUTDIR) ]; then mkdir $(OUTDIR); fi
	$(CC) -o $(OUTDIR)/adapter $(CFLAGS) adapter.cpp
bridge: bridge.cpp
	@if [ ! -d $(OUTDIR) ]; then mkdir $(OUTDIR); fi
	$(CC) -o $(OUTDIR)/bridge $(CFLAGS) bridge.cpp
decorator: decorator.cpp
	@if [ ! -d $(OUTDIR) ]; then mkdir $(OUTDIR); fi
	$(CC) -o $(OUTDIR)/decorator $(CFLAGS) decorator.cpp
facade: facade.cpp
	@if [ ! -d $(OUTDIR) ]; then mkdir $(OUTDIR); fi
	$(CC) -o $(OUTDIR)/facade $(CFLAGS) facade.cpp
factory-method: factory-method.cpp
	@if [ ! -d $(OUTDIR) ]; then mkdir $(OUTDIR); fi
	$(CC) -o $(OUTDIR)/factory-method $(CFLAGS) factory-method.cpp
observer: observer.cpp
	@if [ ! -d $(OUTDIR) ]; then mkdir $(OUTDIR); fi
	$(CC) -o $(OUTDIR)/observer $(CFLAGS) observer.cpp
singleton: singleton.cpp
	@if [ ! -d $(OUTDIR) ]; then mkdir $(OUTDIR); fi
	$(CC) -o $(OUTDIR)/singleton $(CFLAGS) singleton.cpp
strategy: strategy.cpp
	@if [ ! -d $(OUTDIR) ]; then mkdir $(OUTDIR); fi
	$(CC) -o $(OUTDIR)/strategy $(CFLAGS) strategy.cpp
template-method: template-method.cpp
	@if [ ! -d $(OUTDIR) ]; then mkdir $(OUTDIR); fi
	$(CC) -o $(OUTDIR)/template-method $(CFLAGS) template-method.cpp
class: class.cpp
	@if [ ! -d $(OUTDIR) ]; then mkdir $(OUTDIR); fi
	$(CC) -o $(OUTDIR)/class $(CFLAGS) class.cpp


.PHONY: clean
clean:
	rm -rf ./build
