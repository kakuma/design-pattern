#include <iostream>
#include <string>
#include <vector>

// abstract class
class FileSender
{
protected:
  virtual std::string getType() = 0;
  virtual std::vector<uint8_t> formatData() = 0; // 引数は省略
  virtual std::string selectDestination() = 0;
  virtual bool sendData() = 0; // 引数は省略

public:
  void execute()
  {
    std::cout << this->getType() << "でファイルの送信を始めます" << std::endl;

    std::cout << "送信用データの作成" << std::endl;
    this->formatData();

    std::cout << "送信先の設定" << std::endl;
    auto dest = this->selectDestination();

    std::cout << "送信の処理" << std::endl;
    auto res = this->sendData();

    std::cout << dest << (res ? "に送信完了" : "に送信失敗") << std::endl;
  }
};

// concrete class
class UsbFileSender : public FileSender
{
protected:
  std::string getType() override
  {
    return "USB";
  }

  std::vector<uint8_t> formatData() override
  {
    std::cout << "USBパケットに変換" << std::endl;
    auto v = std::vector<uint8_t>();
    v.push_back(0x11);
    return v;
  }

  std::string selectDestination() override
  {
    std::cout << "USBでの送信先の設定" << std::endl;
    return "hoge";
  }

  bool sendData() override
  {
    std::cout << "USBでデータを送信" << std::endl;
    return true;
  }
};

class BtFileSender : public FileSender
{
protected:
  std::string getType() override
  {
    return "Bluetooth";
  }

  std::vector<uint8_t> formatData() override
  {
    std::cout << "Bluetoothパケットに変換" << std::endl;
    auto v = std::vector<uint8_t>();
    v.push_back(0x11);
    return v;
  }

  std::string selectDestination() override
  {
    std::cout << "Bluetoothでの送信先の設定" << std::endl;
    return "fuga";
  }

  bool sendData() override
  {
    std::cout << "Bluetoothでデータを送信" << std::endl;
    return false;
  }
};

int main()
{
  auto usbSender = UsbFileSender();
  auto btSender = BtFileSender();

  usbSender.execute();
  std::cout << "--" << std::endl;
  btSender.execute();
}