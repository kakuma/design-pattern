#include <iostream>
#include <string>

// Component
class Cake
{
public:
  virtual std::string getName() const = 0;
  virtual long int getPrice() const = 0;

  void showInfo() const
  {
    std::cout << this->getPrice() << "円の" << this->getName() << std::endl;
  }
};

// Concrete Component
class PlaneCake : public Cake
{
public:
  std::string getName() const override
  {
    return "ケーキ";
  }

  long int getPrice() const override
  {
    return 100;
  }
};

// Decorator
class DecoratedCake : public Cake
{
protected:
  Cake *cake;
  DecoratedCake(Cake *cake) : cake(cake) {}
};

// ConcreteDecorator
class ShortCake : public DecoratedCake
{
public:
  ShortCake(Cake *cake) : DecoratedCake(cake) {}

  std::string getName() const override
  {
    return "ショート" + this->cake->getName();
  }

  long int getPrice() const override
  {
    return 150 + this->cake->getPrice();
  }
};

class ChocolateCake : public DecoratedCake
{
public:
  ChocolateCake(Cake *cake) : DecoratedCake(cake) {}

  std::string getName() const override
  {
    return "チョコレート" + this->cake->getName();
  }

  long int getPrice() const override
  {
    return 300 + this->cake->getPrice();
  }
};

class StrawberryCake : public DecoratedCake
{
public:
  StrawberryCake(Cake *cake) : DecoratedCake(cake) {}

  std::string getName() const override
  {
    return "イチゴ" + this->cake->getName();
  }

  long int getPrice() const override
  {
    return 5000000000000 + this->cake->getPrice();
  }
};

int main()
{
  auto pc = new PlaneCake;
  auto sc = new ShortCake(pc);
  auto cc = new ChocolateCake(pc);
  auto ssc = new StrawberryCake(sc);

  sc->showInfo();
  cc->showInfo();
  ssc->showInfo();
}
