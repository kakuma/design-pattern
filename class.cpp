#include <iostream>
#include <string>
#include <array>

// abstract class
class Student
{
private:
    std::string name;

public:
    Student(std::string name) : name(name) {}

    std::string getName() const
    {
        return name;
    }

    virtual void study() const = 0;
};

// interface
class IntroduceInterface
{
public:
    virtual void introduce() const = 0;
};

// concrete class
class UndergraduateStudent : public Student
{
public:
    UndergraduateStudent(std::string name) : Student(name) {}

    void study() const override
    {
        std::cout << "study a little" << std::endl;
    }
};

class MasterStudent : public Student, public IntroduceInterface
{
public:
    MasterStudent(std::string name) : Student(name) {}

    void study() const override
    {
        std::cout << "study a lot" << std::endl;
    }

    void introduce() const override
    {
        std::cout << "I am " << this->getName() << ", master student!" << std::endl;
    }
};

class DoctorStudent : public Student
{
public:
    DoctorStudent(std::string name) : Student(name) {}

    void study() const override
    {
        std::cout << "study forever" << std::endl;
    }
};

int main()
{
    auto a = new UndergraduateStudent("hoge");
    auto b = new MasterStudent("fuga");
    auto c = new DoctorStudent("piyo");

    auto labStudents = new std::array<Student *, 3>{a, b, c};

    for (const auto s : *labStudents)
    {
        std::cout << s->getName() << std::endl;
        s->study();
    }

    b->introduce();
}
