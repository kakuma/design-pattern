#include <iostream>
#include <string>
#include <vector>

// Product
class Controler
{
public:
  virtual void sendData(std::vector<u_int8_t> data) = 0;
  virtual bool initialize() const = 0;
};

// Creator
class SendManager
{
protected:
  virtual Controler *createControler() const = 0;

public:
  void send(std::vector<u_int8_t> data) const
  {
    auto c = this->createControler();
    c->initialize();
    c->sendData(data);
  }
};

// Concrete Product
class UsbController : public Controler
{
public:
  bool initialize() const override
  {
    // 初期化処理
    printf("USB: initialized\n");
    return true;
  }

  void sendData(std::vector<u_int8_t> data) override
  {
    // 送信処理
    printf("USB: Data Sended\n");
  };
};

class BtController : public Controler
{
public:
  bool initialize() const override
  {
    // 初期化処理
    printf("Bluetooth: initialized\n");
    return true;
  }

  void sendData(std::vector<u_int8_t> data) override
  {
    // 送信処理
    printf("Bluetooth: Data Sended\n");
  };
};

//Concrete Creator
class UsbSendManager : public SendManager
{
public:
  virtual Controler *createControler() const override
  {
    return new UsbController;
  };
};

class BtSendManager : public SendManager
{
public:
  virtual Controler *createControler() const override
  {
    return new BtController;
  };
};

int main()
{
  SendManager *sm = new BtSendManager();

  auto data = std::vector<uint8_t>();
  data.push_back(0x11);

  sm->send(data);
}