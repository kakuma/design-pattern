#include <iostream>
#include <string>
#include <math.h>

// Strategy
class TaxCalculator
{
public:
  virtual int calcTax(std::string name, int price) const = 0;
};

// ConcreteStrategy
class OldTax : public TaxCalculator
{
public:
  int calcTax(std::string name, int price) const override
  {
    return floor(price * 1.08);
  }
};

class NewTax : public TaxCalculator
{
public:
  int calcTax(std::string name, int price) const override
  {
    // 本当は軽減税率とかの処理がある
    return floor(price * 1.1);
  }
};

//Context
class Context
{
private:
  TaxCalculator *tc;

public:
  Context(TaxCalculator *tc) : tc(tc) {}

  int calcTax(std::string itemName, int price) const
  {
    return this->tc->calcTax(itemName, price);
  }
};

// 関数オブジェクトを使用する場合
class ContextLambda
{
private:
  std::function<int(std::string, int)> tc;

public:
  ContextLambda(std::function<int(std::string, int)> tc) : tc(tc) {}

  int calcTax(std::string itemName, int price) const
  {
    return this->tc(itemName, price);
  }
};

int main()
{
  auto oldSystem = new Context(new OldTax);
  auto newSystem = new Context(new NewTax);

  auto taxSystem = new ContextLambda([&](std::string item, int price) {
    // 本当は軽減税率とかの処理がある
    return floor(price * 1.1);
  });

  auto item = "りんご";
  int price = 100;

  std::cout << item << "は" << oldSystem->calcTax(item, price) << "円から" << newSystem->calcTax(item, price) << "円になります" << std::endl;
  std::cout << item << "は" << taxSystem->calcTax(item, price) << "円です" << std::endl;
}