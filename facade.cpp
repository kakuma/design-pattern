#include <iostream>

// Subsystem
class Car
{
private:
    int speed;
    int distance;

public:
    Car() : speed(0), distance(0) {}

    void setSpeed(int speed)
    {
        this->speed = speed;
    }

    void run(int minutes)
    {
        this->distance += minutes * this->speed;
    }

    int getDistance() const
    {
        return this->distance;
    }
};

class Driver
{
private:
    Car *car;

public:
    Driver(Car *car) : car(car) {}

    void pushPedal(int speed)
    {
        this->car->setSpeed(speed);
    }

    void drive(int minutes)
    {
        this->car->run(minutes);
    }
};

// Facadeクラス（サブシステムのインターフェース）
class DrivingSimulator
{
public:
    void simulate()
    {
        auto c = new Car;
        auto d = new Driver(c);

        d->pushPedal(700);
        d->drive(30);
        d->pushPedal(750);
        d->drive(20);

        std::cout << "The travel distance is " << c->getDistance() << "m." << std::endl;
    }
};

int main()
{
    auto ds = new DrivingSimulator;
    ds->simulate();
}