#include <iostream>
#include <string>
#include <vector>

class Converter
{
public:
  virtual std::vector<uint8_t> convertData() = 0;
};

class DestinationSelector
{
public:
  virtual std::string selectDestination() = 0;
};

class DataSender
{
public:
  virtual bool sendData() = 0;
};

class FileSenderFactory
{
public:
  virtual Converter *createConverter() = 0;
  virtual DestinationSelector *createDestinationSelector() = 0;
  virtual DataSender *createDataSender() = 0;
};

class UsbConverter : public Converter
{
public:
  std::vector<uint8_t> convertData() override
  {
    std::cout << "USB用のデータに変換" << std::endl;
    auto v = std::vector<uint8_t>();
    v.push_back(0x11);
    return v;
  }
};

class UsbDestinationSelector : public DestinationSelector
{
public:
  std::string selectDestination() override
  {
    std::cout << "USBでの送信先の設定" << std::endl;
    return "hoge";
  }
};

class UsbDataSender : public DataSender
{
public:
  bool sendData()
  {
    std::cout << "USBでデータを送信" << std::endl;
    return true;
  }
};

class UsbFileSenderFactory : public FileSenderFactory
{
public:
  Converter *createConverter() override
  {
    return new UsbConverter;
  };

  DestinationSelector *createDestinationSelector() override
  {
    return new UsbDestinationSelector;
  };

  DataSender *createDataSender() override
  {
    return new UsbDataSender;
  }
};

class BtConverter : public Converter
{
public:
  std::vector<uint8_t> convertData() override
  {
    std::cout << "Bluetooth用のデータに変換" << std::endl;
    auto v = std::vector<uint8_t>();
    v.push_back(0x11);
    return v;
  }
};

class BtDestinationSelector : public DestinationSelector
{
public:
  std::string selectDestination() override
  {
    std::cout << "Bluetoothでの送信先の設定" << std::endl;
    return "fuga";
  }
};

class BtDataSender : public DataSender
{
public:
  bool sendData()
  {
    std::cout << "Bluetoothでデータを送信" << std::endl;
    return false;
  }
};

class BtFileSenderFactory : public FileSenderFactory
{
public:
  Converter *createConverter() override
  {
    return new BtConverter;
  };

  DestinationSelector *createDestinationSelector() override
  {
    return new BtDestinationSelector;
  };

  DataSender *createDataSender() override
  {
    return new BtDataSender;
  }
};

int main()
{
  auto usbFac = new UsbFileSenderFactory;
  auto btFac = new BtFileSenderFactory;

  auto execute = [&](FileSenderFactory *fac, std::string type) {
    auto converter = fac->createConverter();
    auto selector = fac->createDestinationSelector();
    auto sender = fac->createDataSender();

    std::cout << type << "でファイルの送信を始めます" << std::endl;
    std::cout << "送信用データの作成" << std::endl;
    converter->convertData();

    std::cout << "送信先の設定" << std::endl;
    auto dest = selector->selectDestination();

    std::cout << "送信の処理" << std::endl;
    auto res = sender->sendData();

    std::cout << dest << (res ? "に送信完了" : "に送信失敗") << std::endl;
  };

  execute(usbFac, "USB");
  std::cout << "--" << std::endl;
  execute(btFac, "Bluetooth");
}