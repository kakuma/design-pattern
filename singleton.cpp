#include <iostream>
#include <string>

class SimpleSingleton
{
private:
  SimpleSingleton() = default;
  ~SimpleSingleton() = default;

public:
  SimpleSingleton(const SimpleSingleton &) = delete;
  SimpleSingleton &operator=(const SimpleSingleton &) = delete;
  SimpleSingleton(SimpleSingleton &&) = delete;
  SimpleSingleton &operator=(SimpleSingleton &&) = delete;

  static SimpleSingleton &getInstance(

  )
  {
    static SimpleSingleton instance;
    return instance;
  }

  const std::string getString() const
  {
    return "goodbye world";
  }
};

int main()
{
  std::cout << SimpleSingleton::getInstance().getString() << std::endl;

  std::cout << &SimpleSingleton::getInstance() << std::endl;
  std::cout << &SimpleSingleton::getInstance() << std::endl;
  std::cout << &SimpleSingleton::getInstance() << std::endl;
}