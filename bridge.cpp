#include <iostream>
#include <string>

// Implementor
class Material
{
public:
  virtual int getParameter() const = 0;
};

// Concrete Implementer
class Wood : public Material
{
public:
  int getParameter() const override
  {
    return 5;
  }
};

class Steel : public Material
{
public:
  int getParameter() const override
  {
    return 30;
  }
};

// Abstraction
class Weapon
{
private:
  Material *material;

public:
  Weapon(Material *material) : material(material) {}

  virtual int getAttack() const = 0;

  virtual int getEndurance() const = 0;

  int getMaterialParameter() const
  {
    return material->getParameter();
  }
};

// Refined Abstraction
class Sword : public Weapon
{
public:
  Sword(Material *material) : Weapon(material) {}

  int getAttack() const override
  {
    return this->getMaterialParameter() * 10;
  }

  int getEndurance() const override
  {
    return this->getMaterialParameter() * 3;
  }
};

class Shield : public Weapon
{
public:
  Shield(Material *material) : Weapon(material) {}

  int getAttack() const override
  {
    return this->getMaterialParameter() * 0.5;
  }

  int getEndurance() const override
  {
    return this->getMaterialParameter() * 10;
  }
};

int main()
{
  auto woodSword = new Sword(new Wood);
  auto steelShield = new Shield(new Steel);

  std::cout << "木刀の攻撃力は" << woodSword->getAttack() << std::endl;
  std::cout << "鉄の盾の耐久力は" << steelShield->getEndurance() << std::endl;
}